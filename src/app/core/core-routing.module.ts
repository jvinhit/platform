import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';
import { AuthModule } from './auth/auth.module';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    // Route to login
    path: 'login',
    component: LoginComponent,
    data: { showHeader: false, showSidebar: false, showFooter: false }
  },
  {
    canActivate: [AuthGuard],
    path: 'dashboard',
    component: DashboardComponent
  }
  // {
  //   // Route to Doctor Area
  //   path: 'doctor',
  //   // loadChildren: () => import('../modules/doctor/doctor.module').then(mod => mod.DoctorModule)
  // },
  // {
  //   // Route to Patient Area
  //   path: 'patient',
  //   // loadChildren: () => import('../modules/patient/patient.module').then(mod => mod.PatientModule)
  // }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules
    }),
    AuthModule
  ],
  exports: [RouterModule]
})
export class CoreRoutingModule {}
