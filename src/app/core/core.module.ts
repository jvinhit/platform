import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { CoreRoutingModule } from './core-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from './layout/layout.module';
import { AuthModule } from './auth/auth.module';

@NgModule({
  declarations: [DashboardComponent, LoginComponent],
  imports: [CommonModule, CoreRoutingModule, SharedModule, FlexLayoutModule, LayoutModule, AuthModule],
  exports: [CoreRoutingModule]
})
export class CoreModule {}
