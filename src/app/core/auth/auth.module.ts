// auth.module.ts ////////////////////////
import { AuthService } from './auth.service';
import { AuthGuardService } from './auth-guard.service';
import { NgModule } from '@angular/core';

@NgModule({
  providers: [AuthService, AuthGuardService]
})
export class AuthModule {}
